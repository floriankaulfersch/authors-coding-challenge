# Coding-Challenge: Autoren-Komponente

## Projekt starten

### `npm install` | `yarn`

Installiert alle dependencies

### `npm start` | `yarn start`

Startet die App im development mode auf port 3000


## Aufgabe

Erstelle eine Autorenliste mit folgenden Vorgaben:
* Alle Autorennamen, die die bereitgestellte Api zurückgibt, werden als Tags dargestellt
* Autoren können per Klick auf das x neben dem Autorennamen gelöscht werden
* Es können neue Autoren hinzugefügt werden, indem ein Name in das Inputfeld eingegeben mit einer der folgenden Aktionen bestätigt wird: 
  * per Enter-Taste
  * per Klick außerhalb des Felds


![goal.png](goal.png)
