const dummyAuthors = ["Tony Stark", "Bruce Banner", "Steve Rogers", "Natasha Romanov", "Thor Odinson"]

export const get = (): Promise<string []> => Promise.resolve(dummyAuthors)
