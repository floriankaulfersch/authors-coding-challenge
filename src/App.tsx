import './App.css';
import { Authors } from "./Authors";

function App() {
  return (
    <Authors />
  );
}

export default App;
